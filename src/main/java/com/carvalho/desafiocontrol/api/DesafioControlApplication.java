package com.carvalho.desafiocontrol.api;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioControlApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(DesafioControlApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
	    // TODO implement profiles
	    
	}
}
