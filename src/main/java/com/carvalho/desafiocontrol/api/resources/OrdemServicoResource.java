package com.carvalho.desafiocontrol.api.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carvalho.desafiocontrol.api.domains.OrdemServico;
import com.carvalho.desafiocontrol.api.domains.Servico;
import com.carvalho.desafiocontrol.api.services.OrdemServicoService;

@RestController
@RequestMapping(value = "/ordemservicos")
public class OrdemServicoResource {

    @Autowired
    OrdemServicoService ordemServicoService;

    @GetMapping("/{id}")
    public ResponseEntity<OrdemServico> getById(@PathVariable Integer id) {
	OrdemServico ordemServico = ordemServicoService.find(id);
	return ResponseEntity.ok().body(ordemServico);
    }

    @GetMapping
    public ResponseEntity<List<OrdemServico>> get() {

	List<OrdemServico> ordemServicos = ordemServicoService.findAll();
	return ResponseEntity.ok().body(ordemServicos);
    }

}
