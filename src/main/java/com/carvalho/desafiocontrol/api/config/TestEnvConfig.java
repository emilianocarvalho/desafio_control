package com.carvalho.desafiocontrol.api.config;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.carvalho.desafiocontrol.api.services.ProfileEnvDBService;

@Configuration
@Profile("test")
public class TestEnvConfig {
    
    @Autowired
    ProfileEnvDBService profileEnvDBService;

    @Bean
    public boolean envDatabase() throws ParseException {
	profileEnvDBService.instantiateTestDatabase();
	return true;
    }
}
