package com.carvalho.desafiocontrol.api.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carvalho.desafiocontrol.api.domains.ItemOrdemServico;
import com.carvalho.desafiocontrol.api.domains.OrdemServico;
import com.carvalho.desafiocontrol.api.domains.Servico;
import com.carvalho.desafiocontrol.api.repositories.ItemOrdemServicoRepository;
import com.carvalho.desafiocontrol.api.repositories.OrdemServicoRepository;
import com.carvalho.desafiocontrol.api.repositories.ServicoRepository;

@Service
public class ProfileEnvDBService {

    @Autowired
    private ServicoRepository servicoRepository;
    
    @Autowired
    private OrdemServicoRepository ordemServicoRepository;
    
    @Autowired
    private ItemOrdemServicoRepository itemOrdemServicoRepository;
    
    public void instantiateTestDatabase() throws ParseException  {

	Servico s1 = new Servico(1, "Corte de Energia", 45.00);
	Servico s2 = new Servico(2, "Inspeção", 20.00);
	Servico s3 = new Servico(3, "Religação de Energia", 30.00);
	
	servicoRepository.saveAll(Arrays.asList(s1, s2, s3));
			 
	String pattern = "MM-dd-yyyy";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	
	String patternT = "HH:mm:ss";
	SimpleDateFormat simpleTimeFormat = new SimpleDateFormat(patternT);	
	
	OrdemServico OS1 = new OrdemServico(null, "Funcionario1", simpleDateFormat.parse("08-22-2018"), simpleTimeFormat.parse("08:00:00"), simpleTimeFormat.parse("18:00:00"), "Sem ocorrências");
	OrdemServico OS2 = new OrdemServico(null, "Funcionario2", simpleDateFormat.parse("08-22-2018"), simpleTimeFormat.parse("08:00:00"), simpleTimeFormat.parse("14:00:00"), "É necessário retornar");
	OrdemServico OS3 = new OrdemServico(null, "Funcionario1", simpleDateFormat.parse("08-23-2018"), simpleTimeFormat.parse("08:00:00"), simpleTimeFormat.parse("18:00:00"), "Sem ocorrências");
	
	
	ordemServicoRepository.saveAll(Arrays.asList(OS1, OS2, OS3));
	
	ItemOrdemServico iOS1 = new ItemOrdemServico(OS1, s1, 15, 45.0, 0.0);
	ItemOrdemServico iOS2 = new ItemOrdemServico(OS2, s2, 8, 20.0, 0.0);
	ItemOrdemServico iOS3 = new ItemOrdemServico(OS3, s3, 50, 30.0, 0.0);

	OS1.getItens().addAll(Arrays.asList(iOS1));
	OS2.getItens().addAll(Arrays.asList(iOS2));
	OS3.getItens().addAll(Arrays.asList(iOS3));
	
	itemOrdemServicoRepository.saveAll(Arrays.asList(iOS1, iOS2, iOS3));

    }
}
