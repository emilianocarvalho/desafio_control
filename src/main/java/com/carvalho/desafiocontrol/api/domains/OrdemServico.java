package com.carvalho.desafiocontrol.api.domains;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class OrdemServico implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String funcionario;

    private Date data;

    private Date horaInicio;

    private Date horaFim;

    private String detalhe;

    @OneToMany(mappedBy = "id.ordemServico")
    private Set<ItemOrdemServico> itens = new HashSet<>();

    public OrdemServico() {
    }

    public OrdemServico(Integer id, String funcionario, Date data, Date horaInicio, Date horaFim,
	    String detalhe) {
	super();
	this.id = id;
	this.funcionario = funcionario;
	this.data = data;
	this.horaInicio = horaInicio;
	this.horaFim = horaFim;
	this.detalhe = detalhe;
    }
    
    public double getTotalOrdemServico() {
	double total = 0.0;
	for (ItemOrdemServico iOS : itens) {
	    total=+ iOS.getValorFinal();
	}
	return total;
    }

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getFuncionario() {
	return funcionario;
    }

    public void setFuncionario(String funcionario) {
	this.funcionario = funcionario;
    }

    public Date getData() {
	return data;
    }

    public void setData(Date data) {
	this.data = data;
    }

    public Date getHoraInicio() {
	return horaInicio;
    }

    public void setHoraInicio(Date horaInicio) {
	this.horaInicio = horaInicio;
    }

    public Date getHoraFim() {
	return horaFim;
    }

    public void setHoraFim(Date horaFim) {
	this.horaFim = horaFim;
    }

    public String getDetalhe() {
	return detalhe;
    }

    public void setDetalhe(String detalhe) {
	this.detalhe = detalhe;
    }

    public Set<ItemOrdemServico> getItens() {
	return itens;
    }

    public void setItens(Set<ItemOrdemServico> itens) {
	this.itens = itens;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	OrdemServico other = (OrdemServico) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}
