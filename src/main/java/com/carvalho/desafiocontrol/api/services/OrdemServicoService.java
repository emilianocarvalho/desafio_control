package com.carvalho.desafiocontrol.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carvalho.desafiocontrol.api.domains.OrdemServico;
import com.carvalho.desafiocontrol.api.repositories.OrdemServicoRepository;

@Service
public class OrdemServicoService {

    @Autowired
    OrdemServicoRepository ordemServicoRepository;
    
    public OrdemServico find(Integer id) {
	Optional<OrdemServico> obj = ordemServicoRepository.findById(id);
	return obj.orElse(null);
    }

    public List<OrdemServico> findAll() {
	return ordemServicoRepository.findAll();
    }
    
}
