package com.carvalho.desafiocontrol.api.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.carvalho.desafiocontrol.api.domains.Servico;
import com.carvalho.desafiocontrol.api.services.ServicoService;

@CrossOrigin
@RestController
@RequestMapping(value = "/servicos")
public class ServicoResources {

    @Autowired
    ServicoService servicoService;

    @GetMapping
    public ResponseEntity<List<Servico>> get() {

	List<Servico> servicos = servicoService.findAll();
	return ResponseEntity.ok().body(servicos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Servico> getById(@PathVariable Integer id) {
	Servico servico = servicoService.find(id);
	return ResponseEntity.ok().body(servico);
    }

    @PostMapping
    public @ResponseBody ResponseEntity<Servico> post(@RequestBody Servico servico, UriComponentsBuilder ucBuilder) {
	servico = servicoService.insert(servico);
	HttpHeaders headers = new HttpHeaders();
	headers.setLocation(ucBuilder.path("/servicos/{id}").buildAndExpand(servico.getId()).toUri());
	return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public @ResponseBody ResponseEntity<String> put(@RequestBody Servico servico, @PathVariable Integer id) {
	servico.setId(id);
	servico = servicoService.update(servico);
	return new ResponseEntity<String>("PUT Response", HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public @ResponseBody ResponseEntity<String> delete(@PathVariable Integer id) {
	servicoService.delete(id);
	return new ResponseEntity<String>("DELETE Response", HttpStatus.OK);
    }

}
