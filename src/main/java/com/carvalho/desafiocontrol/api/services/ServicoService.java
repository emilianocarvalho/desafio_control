package com.carvalho.desafiocontrol.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.carvalho.desafiocontrol.api.domains.Servico;
import com.carvalho.desafiocontrol.api.repositories.ServicoRepository;

@Service
public class ServicoService {

    @Autowired
    ServicoRepository servicoRepository;

    public List<Servico> findAll() {
	return servicoRepository.findAll();
    }

    public Servico find(Integer id) {
	Optional<Servico> obj = servicoRepository.findById(id);
	return obj.orElse(null);
    }

    public Servico findByDescricao(String descricao) {
	Optional<Servico> servico = servicoRepository.findByDescricao(descricao);
	return servico.orElse(null);
    }

    public Servico insert(Servico servico) {
	servico.setId(null);
	return servicoRepository.save(servico);
    }

    public Servico update(Servico servico) {
	find(servico.getId());
	return servicoRepository.save(servico);
    }

    public void delete(Integer id) {
	find(id);
	try {
	    servicoRepository.deleteById(id);
	} catch (DataIntegrityViolationException e) {
	    throw new RuntimeException("Não foi possivel excluir o serviço!");
	}
    }

}
