package com.carvalho.desafiocontrol.api.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.carvalho.desafiocontrol.api.domains.Servico;

@Repository
public interface ServicoRepository extends JpaRepository<Servico, Integer>{

    @Transactional(readOnly=true)
    Optional<Servico> findByDescricao(String descricao);
}
