package com.carvalho.desafiocontrol.api.domains;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class ItemOrdemServico implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonIgnore
    @EmbeddedId
    private ItemServicoPK id = new ItemServicoPK();

    private int quantidade;
    @Column(nullable = false, precision = 8, scale = 2)
    private double valorUnitario;
    @Column(precision = 8, scale = 2)    
    private double desconto;

    public ItemOrdemServico() {
    }

    public ItemOrdemServico(OrdemServico ordemServico, Servico servico, int quantidade, double valorUnitario,
	    double desconto) {
	super();
	id.setOrdemServico(ordemServico);
	id.setServico(servico);
	this.quantidade = quantidade;
	this.valorUnitario = valorUnitario;
	this.desconto = this.getDescontoAplicado() * 100.0;
    }
    
    public double getValorTotal() {
	return valorUnitario * quantidade;
    }
    
    public double getDescontoAplicado() {
	if (quantidade >= 30) {
	    desconto = 0.3;
	} else if (quantidade >= 20.0) {
	    desconto = 0.2;
	} else if (quantidade >= 10.0) {
	    desconto = 0.1;
	}
	return desconto;
    }

    public double getValorFinal() {
	double valorDesconto = this.getValorTotal() * this.getDescontoAplicado();	
	return this.getValorTotal() - valorDesconto;
    }
    
    @JsonIgnore
    public OrdemServico getOrdemServico() {
	return id.getOrdemServico();
    }

    
    public Servico getServico() {
	return id.getServico();
    }

    public ItemServicoPK getId() {
	return id;
    }

    public void setId(ItemServicoPK id) {
	this.id = id;
    }

    public int getQuantidade() {
	return quantidade;
    }

    public void setQuantidade(int quantidade) {
	this.quantidade = quantidade;
    }

    public double getValorUnitario() {
	return valorUnitario;
    }

    public void setValorUnitário(double valorUnitario) {
	this.valorUnitario = valorUnitario;
    }

    public double getDesconto() {
	return desconto;
    }

    public void setDesconto(double desconto) {
	this.desconto = desconto;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	ItemOrdemServico other = (ItemOrdemServico) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}
