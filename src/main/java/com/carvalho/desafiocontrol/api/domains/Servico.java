package com.carvalho.desafiocontrol.api.domains;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Servico implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String descricao;

    private Double valor;

    @JsonIgnore
    @OneToMany(mappedBy = "id.servico")
    private Set<ItemOrdemServico> itens = new HashSet<>();

    public Servico() {
    }

    public Servico(Integer id, String descricao, Double valor) {
	super();
	this.id = id;
	this.descricao = descricao;
	this.valor = valor;
    }
    
    @JsonIgnore
    public List<OrdemServico> getOrdemServicos() {
	List<OrdemServico> lista = new ArrayList<>();
	for (ItemOrdemServico x : itens) {
	    lista.add(x.getOrdemServico());
	}
	return lista;
    }

    public Integer getId() {
	return id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getDescricao() {
	return descricao;
    }

    public void setDescricao(String descricao) {
	this.descricao = descricao;
    }

    public Double getValor() {
	return valor;
    }

    public void setValor(Double valor) {
	this.valor = valor;
    }

    public Set<ItemOrdemServico> getItens() {
	return itens;
    }

    public void setItens(Set<ItemOrdemServico> itens) {
	this.itens = itens;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Servico other = (Servico) obj;
	if (descricao == null) {
	    if (other.descricao != null)
		return false;
	} else if (!descricao.equals(other.descricao))
	    return false;
	return true;
    }

}
